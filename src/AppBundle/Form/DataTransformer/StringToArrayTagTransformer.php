<?php
declare(strict_types=1);

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToArrayTagTransformer implements DataTransformerInterface
{
    /** @var string */
    protected $separator;

    public function __construct(string $separator)
    {
        $this->separator = $separator;
    }

    /** @var $value string */
    public function reverseTransform($value): array
    {
        if (!is_string($value)) {
            return [];
        }
        $values = explode($this->separator, $value);

        $trimmedValues = array_map('trim', $values);
        $valuesWithoutEmptyEntries = array_diff($trimmedValues, ['']);
        $uniqueValues = array_values(array_unique($valuesWithoutEmptyEntries));

        return $uniqueValues ?? [];
    }

    /** @var $value array */
    public function transform($value): string
    {
        if (!is_array($value)) {
            return '';
        }
        return implode($this->separator, $value);
    }
}