<?php
declare(strict_types=1);

namespace AppBundle\Form\Type;

use AppBundle\Entity\BlogPost;
use AppBundle\Form\DataTransformer\StringToArrayTagTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogPostType extends AbstractType
{
    private const TAGS_SEPARATOR = ';';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, ['required' => true])
            ->add('content', TextType::class, ['required' => true])
            ->add('tags', TextType::class, ['required' => false]);

        $builder->get('tags')->addModelTransformer(new StringToArrayTagTransformer(self::TAGS_SEPARATOR));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => BlogPost::class,
            ]
        );
    }
}