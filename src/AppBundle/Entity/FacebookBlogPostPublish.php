<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="blog_post_publish")
 * @ORM\Entity()
 */
class FacebookBlogPostPublish extends BlogPostPublish
{
    public function getType(): string
    {
        return BlogPostPublish::FACEBOOK_TYPE;
    }
}