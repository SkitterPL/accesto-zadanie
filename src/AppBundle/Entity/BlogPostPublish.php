<?php
declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Table(name="blog_post_publish",indexes={
 *     @ORM\Index(name="type_idx", columns={"type"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BlogPostPublishRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap(
 *     {
 *     BlogPostPublish::FACEBOOK_TYPE = "FacebookBlogPostPublish",
 *     BlogPostPublish::TWITTER_TYPE = "TwitterBlogPostPublish",
 *     }
 * )
 */
abstract class BlogPostPublish implements BlogPostPublishInterface
{
    public const FACEBOOK_TYPE = 'facebook';
    public const TWITTER_TYPE = 'twitter';

    public const PUBLISHED_STATUS = 'published';
    public const NEW_STATUS = 'new';


    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var BlogPost
     * @ORM\ManyToOne(targetEntity="BlogPost")
     * @ORM\JoinColumn(name="blog_post_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $blogPost;

    /**
     * @var \DateTime | null
     * @ORM\Column(type="datetime")
     */
    protected $publishedAt;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    protected $status = self::NEW_STATUS;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlogPost(): BlogPost
    {
        return $this->blogPost;
    }

    public function setBlogPost(BlogPost $blogPost): void
    {
        $this->blogPost = $blogPost;
    }

    public function getPublishedAt(): \DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}