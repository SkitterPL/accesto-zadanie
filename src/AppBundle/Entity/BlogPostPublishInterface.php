<?php
declare(strict_types=1);

namespace AppBundle\Entity;

interface BlogPostPublishInterface
{
    public function getType(): string;
}