<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\BlogPost;
use AppBundle\Exception\TargetNotExistsException;
use AppBundle\Form\Type\BlogPostType;
use AppBundle\Service\Publisher\BlogPostPublisherInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogPostController extends FOSRestController
{
    /**
     * @Rest\Get("/blog-post")
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Return complete list of blog posts"
     * )
     * @Security("has_role('ROLE_ADMIN') || has_role('ROLE_USER')")
     */
    public function listPostsAction(): View
    {
        $blogPostRepository = $this->getDoctrine()->getRepository(BlogPost::class);
        return new View($blogPostRepository->findAll(), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/blog-post")
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Create new blog post",
     *     input="AppBundle\Form\Type\BlogPostType",
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function postAction(
        Request $request,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager): View
    {
        $blogPost = new BlogPost();
        $form = $formFactory->create(BlogPostType::class, $blogPost);
        $form->submit($request->get($form->getName()));

        if (!$form->isValid()) {
            return new View($form, Response::HTTP_BAD_REQUEST);
        }

        $entityManager->persist($blogPost);
        $entityManager->flush();;


        return new View(['id' => $blogPost->getId()], Response::HTTP_CREATED);
    }

    /**
     * @Rest\Patch("/blog-post/{blog_post_id}", requirements={"blog_post_id" = "\d+"})
     * @ParamConverter("blogPost", options={"id" = "blog_post_id"})
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Update blog post",
     *     input="AppBundle\Form\Type\BlogPostType",
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function putAction(BlogPost $blogPost, Request $request, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager): View
    {
        $form = $formFactory->create(BlogPostType::class, $blogPost);
        $form->submit($request->get($form->getName()), false);

        if (!$form->isValid()) {
            return new View($form, Response::HTTP_BAD_REQUEST);
        }

        $entityManager->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Post("/blog-post/{blog_post_id}/{target}", requirements={"blog_post_id" = "\d+"})
     * @ParamConverter("blogPost", options={"id" = "blog_post_id"})
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Publish post to specified target. Currently available targets: facebook, twitter"
     * )
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function publishPostAction(BlogPost $blogPost, string $target, BlogPostPublisherInterface $blogPostPublisher): View
    {
        try {
            $blogPostPublisher->publish($blogPost, $target);
        } catch (TargetNotExistsException $exception) {
            return new View(['error' => "This target is not supported"], Response::HTTP_BAD_REQUEST);
        }
        return new View(null, Response::HTTP_NO_CONTENT);
    }
}
