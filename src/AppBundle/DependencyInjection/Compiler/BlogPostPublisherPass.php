<?php
declare(strict_types=1);

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Service\Publisher\BlogPostPublisher;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BlogPostPublisherPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(BlogPostPublisher::class)) {
            return;
        }

        $definition = $container->findDefinition(BlogPostPublisher::class);

        $taggedServices = $container->findTaggedServiceIds('app.blog_post_producer');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addProducer', [
                    new Reference($id),
                    $attributes['alias']
                ]);
            }
        }
    }
}