<?php
declare(strict_types=1);

namespace AppBundle\Service\Factory;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;
use AppBundle\Entity\BlogPostPublishInterface;

class BlogPostPublishFactory implements BlogPostPublishFactoryInterface
{
    public function create(BlogPost $blogPost, string $publishClassName): BlogPostPublish
    {
        try {
            $reflection = new \ReflectionClass($publishClassName);
            if (!$reflection->implementsInterface(BlogPostPublishInterface::class)) {
                throw new \InvalidArgumentException();
            }
        } catch (\ReflectionException $reflectionException) {
            throw new \InvalidArgumentException();
        }

        /** @var BlogPostPublish $blogPostPublish */
        $blogPostPublish = new $publishClassName;
        $blogPostPublish->setBlogPost($blogPost);

        return $blogPostPublish;
    }
}