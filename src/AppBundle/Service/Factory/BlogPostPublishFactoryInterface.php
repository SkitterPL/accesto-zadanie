<?php
declare(strict_types=1);

namespace AppBundle\Service\Factory;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;

interface BlogPostPublishFactoryInterface
{
    public function create(BlogPost $blogPost, string $publishClassName): BlogPostPublish;
}