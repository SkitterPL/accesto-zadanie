<?php
declare(strict_types=1);

namespace AppBundle\Service\Publisher;

use AppBundle\Entity\BlogPost;
use AppBundle\Exception\TargetNotExistsException;
use AppBundle\Service\Publisher\Producers\BlogPostProducerInterface;

class BlogPostPublisher implements BlogPostPublisherInterface
{
    /** @var array */
    protected $producers;

    public function __construct()
    {
        $this->producers = [];
    }

    public function addProducer(BlogPostProducerInterface $blogPostProducer, string $alias): void
    {
        $this->producers[$alias] = $blogPostProducer;
    }

    public function publish(BlogPost $blogPost, string $publishType): void
    {
        if (!isset($this->producers[$publishType])) {
            throw new TargetNotExistsException();
        }

        /** @var BlogPostProducerInterface $producer */
        $producer = $this->producers[$publishType];

        $producer->publish($blogPost);

        return;
    }
}