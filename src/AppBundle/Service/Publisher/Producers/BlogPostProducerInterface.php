<?php
declare(strict_types=1);

namespace AppBundle\Service\Publisher\Producers;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;

interface BlogPostProducerInterface
{
    public function publish(BlogPost $blogPost): BlogPostPublish;
}