<?php
declare(strict_types=1);

namespace AppBundle\Service\Publisher\Producers;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;
use AppBundle\Entity\FacebookBlogPostPublish;
use AppBundle\Service\Factory\BlogPostPublishFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class FacebookBlogPostProducer implements BlogPostProducerInterface
{
    /** @var BlogPostPublishFactoryInterface */
    private $blogPostPublishFactory;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        BlogPostPublishFactoryInterface $blogPostPublishFactory,
        EntityManagerInterface $entityManager)
    {
        $this->blogPostPublishFactory = $blogPostPublishFactory;
        $this->entityManager = $entityManager;
    }

    public function publish(BlogPost $blogPost): BlogPostPublish
    {
        $blogPostPublish = $this->blogPostPublishFactory->create($blogPost, FacebookBlogPostPublish::class);
        $blogPostPublish->setStatus(BlogPostPublish::PUBLISHED_STATUS);
        $blogPostPublish->setPublishedAt(new \DateTime());
        $this->entityManager->persist($blogPostPublish);
        $this->entityManager->flush();
        return $blogPostPublish;
    }
}