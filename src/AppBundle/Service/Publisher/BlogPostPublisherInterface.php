<?php
declare(strict_types=1);

namespace AppBundle\Service\Publisher;

use AppBundle\Entity\BlogPost;
use AppBundle\Exception\TargetNotExistsException;

interface BlogPostPublisherInterface
{
    /** @throws TargetNotExistsException */
    public function publish(BlogPost $blogPost, string $publishType): void;
}