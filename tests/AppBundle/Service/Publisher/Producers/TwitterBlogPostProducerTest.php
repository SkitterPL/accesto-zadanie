<?php
declare(strict_types=1);

namespace Tests\AppBundle\Service\Publisher\Producers;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;
use AppBundle\Service\Factory\BlogPostPublishFactory;
use AppBundle\Service\Publisher\Producers\TwitterBlogPostProducer;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TwitterBlogPostProducerTest extends TestCase
{

    /** @var EntityManagerInterface | MockObject */
    protected $entityManagerMock;

    /** @var TwitterBlogPostProducer */
    protected $twitterBlogPostProducer;

    public function setUp()
    {
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $this->twitterBlogPostProducer = new TwitterBlogPostProducer(new BlogPostPublishFactory(), $this->entityManagerMock);
    }

    /** @test */
    public function shouldPublishBlogPostWithSuccess(): void
    {
        $this->entityManagerMock
            ->expects($this->once())
            ->method('persist');

        $this->entityManagerMock
            ->expects($this->once())
            ->method('flush');

        $blogPostPublish = $this->twitterBlogPostProducer->publish(new BlogPost());
        $this->assertEquals(BlogPostPublish::PUBLISHED_STATUS, $blogPostPublish->getStatus());
        $this->assertEquals(BlogPostPublish::TWITTER_TYPE, $blogPostPublish->getType());
        $this->assertNotNull($blogPostPublish->getPublishedAt());
    }

}