<?php
declare(strict_types=1);

namespace Tests\AppBundle\Service\Publisher\Producers;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\BlogPostPublish;
use AppBundle\Service\Factory\BlogPostPublishFactory;
use AppBundle\Service\Publisher\Producers\FacebookBlogPostProducer;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FacebookBlogPostProducerTest extends TestCase
{

    /** @var EntityManagerInterface | MockObject */
    protected $entityManagerMock;

    /** @var FacebookBlogPostProducer */
    protected $facebookBlogPostProducer;

    public function setUp()
    {
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $this->facebookBlogPostProducer = new FacebookBlogPostProducer(new BlogPostPublishFactory(), $this->entityManagerMock);
    }

    /** @test */
    public function shouldPublishBlogPostWithSuccess(): void
    {
        $this->entityManagerMock
            ->expects($this->once())
            ->method('persist');

        $this->entityManagerMock
            ->expects($this->once())
            ->method('flush');

        $blogPostPublish = $this->facebookBlogPostProducer->publish(new BlogPost());
        $this->assertEquals(BlogPostPublish::FACEBOOK_TYPE, $blogPostPublish->getType());
        $this->assertNotNull($blogPostPublish->getPublishedAt());
    }

}