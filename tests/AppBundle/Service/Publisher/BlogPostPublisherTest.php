<?php
declare(strict_types=1);

namespace Tests\AppBundle\Service\Publisher;

use AppBundle\Entity\BlogPost;
use AppBundle\Exception\TargetNotExistsException;
use AppBundle\Service\Publisher\BlogPostPublisher;
use AppBundle\Service\Publisher\BlogPostPublisherInterface;
use AppBundle\Service\Publisher\Producers\FacebookBlogPostProducer;
use AppBundle\Service\Publisher\Producers\TwitterBlogPostProducer;
use PHPUnit\Framework\TestCase;

class BlogPostPublisherTest extends TestCase
{
    /** @var BlogPostPublisherInterface */
    protected $blogPostPublisher;

    public function setUp()
    {
        $facebookProducerMock = $this->createMock(FacebookBlogPostProducer::class);
        $twitterProducerMock = $this->createMock(TwitterBlogPostProducer::class);
        $this->blogPostPublisher = new BlogPostPublisher();
        $this->blogPostPublisher->addProducer($facebookProducerMock, 'facebook');
        $this->blogPostPublisher->addProducer($twitterProducerMock, 'twitter');
    }

    /**
     * @dataProvider invalidDataProvider
     * @test
     */
    public function shouldThrowTargetNotExistsExceptionOnCreateBlogPostPublish(BlogPost $blogPost, string $target): void
    {
        $this->expectException(TargetNotExistsException::class);
        $this->blogPostPublisher->publish($blogPost, $target);
    }

    public function invalidDataProvider(): array
    {
        return [
            'google' => [new BlogPost(), 'google'],
        ];
    }

}