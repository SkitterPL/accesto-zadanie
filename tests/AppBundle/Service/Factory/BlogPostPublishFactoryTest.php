<?php
declare(strict_types=1);

namespace Tests\AppBundle\Service\Factory;

use AppBundle\AppBundle;
use AppBundle\Entity\BlogPost;
use AppBundle\Entity\FacebookBlogPostPublish;
use AppBundle\Entity\TwitterBlogPostPublish;
use AppBundle\Service\Factory\BlogPostPublishFactory;
use PHPUnit\Framework\TestCase;

class BlogPostPublishFactoryTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     * @test
     */
    public function shouldReturnValidBlogPostPublish(BlogPost $blogPost, string $publishClassName): void
    {
        $blogPostPublishFactory = new BlogPostPublishFactory();
        $blogPostPublish = $blogPostPublishFactory->create($blogPost, $publishClassName);

        $this->assertInstanceOf($publishClassName, $blogPostPublish);
        $this->assertEquals($blogPost, $blogPostPublish->getBlogPost());
    }

    /**
     * @dataProvider invalidDataProvider
     * @test
     */
    public function shouldThrowInvalidArgumentExceptionOnCreateBlogPostPublish(BlogPost $blogPost, string $publishClassName): void
    {
        $blogPostPublishFactory = new BlogPostPublishFactory();
        $this->expectException(\InvalidArgumentException::class);
        $blogPostPublishFactory->create($blogPost, $publishClassName);
    }

    public function validDataProvider(): array
    {
        return [
            'facebook' => [new BlogPost(), FacebookBlogPostPublish::class],
            'twitter' => [new BlogPost(), TwitterBlogPostPublish::class]
        ];
    }

    public function invalidDataProvider(): array
    {
        return [
            'invalid_class_name' => [new BlogPost(), 'google_class'],
            'class_name_not_blog_post_publish' => [new BlogPost(), AppBundle::class]
        ];
    }
}