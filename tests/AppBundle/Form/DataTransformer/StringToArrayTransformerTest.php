<?php
declare(strict_types=1);

namespace Tests\AppBundle\Form\DataTransformer;

use AppBundle\Form\DataTransformer\StringToArrayTagTransformer;
use PHPUnit\Framework\TestCase;

class StringToArrayTransformerTest extends TestCase
{
    /** @dataProvider transformDataProvider */
    public function testTransform(string $separator, array $textToTransform, string $expectedResult): void
    {
        $transformer = new StringToArrayTagTransformer($separator);
        $result = $transformer->transform($textToTransform);
        $this->assertEquals($expectedResult, $result);
    }

    /** @dataProvider reverseTransformDataProvider */
    public function testReverseTransform(string $separator, string $arrayToTransform, array $expectedResult): void
    {
        $transformer = new StringToArrayTagTransformer($separator);
        $result = $transformer->reverseTransform($arrayToTransform);
        $this->assertEquals($expectedResult, $result);
    }

    public function reverseTransformDataProvider()
    {
        return [
            [';', 'tag_1;tag_2;tag_3', ['tag_1', 'tag_2', 'tag_3']],
            [';', 'tag_1           ;    tag2 ;tag 3', ['tag_1', 'tag2', 'tag 3']],
            [',', 'tag_2;tag1;tag3', ['tag_2;tag1;tag3']],
            [';', 'Tag 1', ['Tag 1']],
            [';', 'Tag 1; Tag 1', ['Tag 1']]
        ];
    }

    public function transformDataProvider()
    {
        return [
            [';', ['tag_1', 'tag_2', 'tag_3'], 'tag_1;tag_2;tag_3'],
            [';', ['tag_1', 'tag2', 'tag 3'], 'tag_1;tag2;tag 3'],
            [',', ['tag_2;tag1;tag3'], 'tag_2;tag1;tag3'],
            [';', ['Tag 1'], 'Tag 1']
        ];
    }

}